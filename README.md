# Laravel CRUD Scaffold

A Laravel 5 package to help you generate nearly complete CRUD pages like crazy!

`php artisan mbt:scaffold Model`